#include "comm.hpp"
#include "log.hpp"

int main()
{
    //获取项目key值
    key_t key = ftok(PATH, PROJ_ID);
    if (key == -1)
    {
        perror("ftok");
    }

    log("get a only of key:") << key << std::endl;    
    sleep(5);

    //创建共享内存
    int shmid = shmget(key, SIZE, IPC_CREAT | IPC_EXCL | 0666);
    if (shmid == -1)
    {
        perror("shmget");
    }

    log("creat a shm id is:") << shmid << std::endl;
    sleep(5);

    //挂接到共享内存
    char* shmaddr = (char*)shmat(shmid, nullptr, 0);
    assert(shmaddr != (void*)-1);

    log("link this porcess:") << shmaddr << std::endl;
    sleep(5);

    //通信处理

    //断开挂接
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;

    log("Off this porcess:") << n << std::endl;
    sleep(5);

    //删除共享内存
    int s = shmctl(shmid, IPC_RMID, 0);
    assert(s != -1);
    (void)s;

    log("delete this shm:") << s << std::endl;
    sleep(5);

    return 0;
}