#ifndef _LOG_HPP
#define _LOG_HPP

#include <iostream>
#include <string>
#include <ctime>

std::ostream &log(std::string message)
{
    std::cout << (unsigned int)time(nullptr) << ":" << message;
    return std::cout;
}

#endif