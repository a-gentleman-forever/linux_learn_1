#include "comm.hpp"

int main()
{
    // 获取项目key值
    key_t key = ftok(PATH, PROJ_ID);
    if (key == -1)
    {
        perror("ftok");
    }

    // 获取共享内存
    int shmid = shmget(key, SIZE, 0);
    if (shmid == -1)
    {
        perror("shmget");
    }

    //挂接
    char* shmaddr = (char*)shmat(shmid, nullptr, 0);

    sleep(5);

     //断开挂接
    int n = shmdt(shmaddr);
    assert(n != -1);
    (void)n;

    return 0;
}