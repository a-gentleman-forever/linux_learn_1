#include "ProcBar.h"

void procces()
{
  char a[NUM];
  memset(a, 0, sizeof(a));

  const char* end = "-/|\\";
  int i = 0;
  while (i <= 100)
  {
    printf("[%-100s][%d%%][%c]\r", a, i, end[i%4]);
    fflush(stdout);
    a[i++] = '#';
    usleep(30000);
  }
  printf("\n");
}

