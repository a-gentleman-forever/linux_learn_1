#pragma once

#include "log.hpp"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unordered_map>
#include <unistd.h>
#include <cstring>

class UdpServer
{
public:
    UdpServer(uint16_t port, std::string ip = "")
    :_port(port)
    ,_ip(ip)
    {}

    void initServer()
    {
        // 创建套接字
        _sock = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sock < 0)
        {
            exit(2);
        }

        sockaddr_in local;
        bzero(&local, sizeof local);

        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        // bind绑定 IP + prot
        if (bind(_sock, (struct sockaddr*)&local, sizeof local) > 0)
        {
            exit(-2);
        }

        logMessage(NORMAL, "server is init success!");
    }

    void start()
    {
        char buffer[1024];
        struct sockaddr_in peer;
        socklen_t len = sizeof peer;

        while (true)
        {
            std::string cli_ip;
            uint16_t cli_port = 0;
            char key[64];

            ssize_t s = recvfrom(_sock, buffer, sizeof buffer, 0, (struct sockaddr*)&peer, &len);
            if (s > 0)
            {
                cli_ip = inet_ntoa(peer.sin_addr);
                cli_port = ntohs(peer.sin_port);

                snprintf(key, sizeof key, "%s-%u", cli_ip.c_str(), cli_port);

                auto it = _users.find(key);
                if (it == _users.end())
                {
                    _users.insert({key, peer});
                    logMessage(NORMAL, "add a new user:%s", key);
                }

            }

            sendto(_sock, buffer, sizeof buffer, 0, (struct sockaddr*)&peer, len);
            logMessage(NORMAL, "push message to %s", key);
        }
    }

    ~UdpServer()
    {
        close(_sock);
    }

private:
    int _sock;
    std::string _ip;
    uint16_t _port;
    std::unordered_map<std::string, struct sockaddr_in> _users;
};