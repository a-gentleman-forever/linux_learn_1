#pragma once

// 日志等级

#include <iostream>
#include <ctime>
#include <string>
#include <cstdarg>
#include <cstdio>

#define DEBUG   0
#define NORMAL  1
#define WORNING 2
#define ERROR   3
#define FATAL   4

// 日志等级映射
const char* g_Level[] = {
    "DEBUG",
    "NORMAL",
    "WORNING",
    "ERROE",
    "FATAL"
};

void logMessage(int level, const char* format, ... )
{
#ifndef SHOWDBUGE
    if (level == DEBUG)
    {   
        return;
    }
#endif

    char stdBuffer[1024];
    char logBuffer[1024];

    time_t timestamp = time(nullptr);
    struct tm* curtm = gmtime(&timestamp);

    snprintf(stdBuffer, sizeof stdBuffer, "[%s] [%d年%d月%d日%d时%d分%d秒] ", g_Level[level], 
    curtm->tm_year + 1900, curtm->tm_mon + 1, curtm->tm_mday, curtm->tm_hour + 1 + 7 % 24, curtm->tm_min, curtm->tm_sec);

    va_list args;

    va_start(args, format);
    vsnprintf(logBuffer, sizeof(logBuffer), format, args);
    va_end(args);

    printf("%s:%s\n", stdBuffer, logBuffer);
}