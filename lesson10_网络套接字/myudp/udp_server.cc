#include <iostream>
#include "udp_server.hpp"

int main(int args, char* argv[])
{
    if (args != 2)
    {
        logMessage(FATAL, "erro args != 2");
        exit(2);
    }

    uint16_t prot = atoi(argv[1]);
    UdpServer* server = (new UdpServer(prot));
    
    server->initServer();
    server->start();

    return 0;
}