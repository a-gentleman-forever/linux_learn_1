#ifndef __UDP_SERVER_HPP
#define __UDP_SERVER_HPP

#include "log.hpp"
#include <cstring>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <unordered_map>
#include <pthread.h>

class UdpServer
{
public:
    UdpServer(uint16_t port, std::string ip = "")
        : _port(port), _ip(ip), _sock(-1)
    {
    }

    bool initServer()
    {
        // 1. 创建套接字
        _sock = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sock < 0)
        {
            logMessage(FATAL, "%d:%s\n", errno, strerror(errno));
            exit(2);
        }
        // 2. bind绑定端口
        struct sockaddr_in local;
        bzero(&local, sizeof(local));

        // 配置 ip 端口
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());

        // bind 绑定
        if (bind(_sock, (struct sockaddr *)&local, sizeof local) < 0)
        {
            logMessage(FATAL, "%d:%s\n", errno, strerror(errno));
            exit(2);
        }
        logMessage(NORMAL, "init udp server done ... %s\n", strerror(errno));

        return true;
    }

    void start()
    {
        // 服务器开始，作为一款服务器是永不退出的
        // 实现：echo server: client 给我发送信息，我们返回这条信息
        char buffer[1024];
        while (true)
        {
            struct sockaddr_in peer;
            bzero(&peer, sizeof peer);
            char key[64];

            socklen_t len = sizeof peer;
            // 读取数据 //分析处理数据 // 阻塞
            ssize_t s = recvfrom(_sock, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
            if (s > 0)
            {
                logMessage(DEBUG, "服务器收到数据\n");
                buffer[s] = 0;
                uint16_t cli_port = ntohs(peer.sin_port);
                std::string cli_ip = inet_ntoa(peer.sin_addr);
                // printf("[%s:%u]: %s\n", cli_ip.c_str(), cli_port, buffer);
                snprintf(key, sizeof key, "%s-%u", cli_ip.c_str(), cli_port);
                auto it = _users.find(key);
                if (it == _users.end())
                {
                    logMessage(NORMAL, "add new user: %s", key);
                    _users.insert({key, peer});
                }
            }

            // 写回数据
            for (auto& iter : _users)
            {
                std::string massage = key;
                massage += "#";
                massage += buffer;
                logMessage(NORMAL, "push message to %s", iter.first.c_str());
                sendto(_sock, massage.c_str(), massage.size(), 0, (struct sockaddr *)&(iter.second), sizeof(iter.second));
            }
        }
    }

    ~UdpServer()
    {
        if (_sock >= 0)
            close(_sock);
    }

private:
    // 一个服务器需要一个 ip地址 port端口
    uint16_t _port;
    std::string _ip;
    int _sock;
    std::unordered_map<std::string, struct sockaddr_in> _users;
};

#endif