#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "thread.hpp"
#include <memory>
#include <cstdlib>
#include <cstdio>
#include <string.h>

uint16_t serverport = 0;
std::string serverip;
struct sockaddr_in server; 

void usage(std::string proc)
{
    std::cout << "./nUsage" << proc << "ip prot\n" << std::endl;
}

static void* udpSend(void* args)
{
    int sock = *(int*)(((ThreadData*)args)->_args);
    std::string name = ((ThreadData*)args)->_name;

    std::string message;
    while (true)
    {
        std::cout << "请输入消息：";
        std::getline(std::cin, message);
        if (message == "quit")
        {
            break;
        }
        // 第一次向服务器发送消息的时候OS会自动分配prot和IP
        sendto(sock, message.c_str(), message.size(), 0, (struct sockaddr*)&server, sizeof server);
    }
}

static void* udpRecv(void* args)
{
    int sock = *(int*)(((ThreadData*)args)->_args);
    std::string name = ((ThreadData*)args)->_name;

    char buffer[1024];
    while (true)
    {
        memset(buffer, 0, sizeof(buffer));
        struct sockaddr_in temp;
        socklen_t len = sizeof temp;
        ssize_t s = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr*)&temp, &len);
        if (s > 0)
        {
            buffer[s] = 0;
            std::cout << buffer << std::endl;
        }
    }
}

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        usage(argv[0]);
        exit(1);
    }
    // 客户端创建套接字
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        std::cerr << "socket error" << std::endl;
        exit(2);
    }
    
    serverport = atoi(argv[2]);
    serverip = argv[1];

    // client要不要bind？？要，但是一般client不会显示的bind，程序员不会自己bind
    // client是一个客户端 -> 普通人下载安装启动使用的-> 如果程序员自己bind了->
    // client 一定bind了一个固定的ip和port，万一，其他的客户端提前占用了这个port呢？？
    // client一般不需要显示的bind指定port，而是让OS自动随机选择(什么时候做的呢？)
    char buffer[1024];
    std::string message;
    server.sin_family = AF_INET;
    server.sin_port = htons(serverport);
    server.sin_addr.s_addr = inet_addr(serverip.c_str());

    std::unique_ptr<Thread> sender(new Thread(1, udpSend, (void*)&sock));
    std::unique_ptr<Thread> recver(new Thread(2, udpRecv, (void*)&sock));
    
    sender->start();
    recver->start();

    sender->join();
    recver->join();

    // while (true)
    // {
    //     std::cout << "请输入消息：";
    //     std::getline(std::cin, message);
    //     if (message == "quit")
    //     {
    //         break;
    //     }
    //     // 第一次向服务器发送消息的时候OS会自动分配prot和IP
    //     sendto(sock, message.c_str(), message.size(), 0, (struct sockaddr*)&server, sizeof server);

    //     // 接收消息
    //     struct sockaddr_in temp;
    //     socklen_t len = sizeof temp;
    //     ssize_t s = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr*)&temp, &len);
    //     if (s > 0)
    //     {
    //         buffer[s] = 0;
    //         std::cout << "server echo: " << buffer << std::endl;
    //     }
    // }   

    close(sock);

    return 0;
}