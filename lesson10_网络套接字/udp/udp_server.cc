#include "udp_server.hpp"
#include <memory>
#include <cstdlib>

void usage(std::string proc)
{
    std::cout << "\nUsage: " << proc << " prot\n" << std::endl;
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        usage(argv[0]);
        exit(1);
    }

    uint16_t port = atoi(argv[1]);
    // std::string ip = argv[2];
    // std::unique_ptr<UdpServer> svr(new UdpServer(port, ip));
    std::unique_ptr<UdpServer> svr(new UdpServer(port));
    svr->initServer();
    svr->start();
    
    return 0;
}