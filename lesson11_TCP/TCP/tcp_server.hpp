#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <cassert>
#include <signal.h>
#include <unordered_map>
#include <memory>
#include <sys/wait.h>
#include "lesson9_线程池/log.hpp"
#include "lesson9_线程池/thread.hpp"
#include "lesson9_线程池/threadPool.hpp"
#include "lesson9_线程池/Task.hpp"

// class ThreadData
// {
// public:
//     int _sock;
//     std::string _ip;
//     uint16_t _port;
// };

static void service(int sock, const std::string &clientip,
                    const uint16_t &clientport, const std::string &thread_name)
{
    // echo server
    //  同时在线10人
    //  所以，我们一般服务器进程业务处理，如果是从连上，到断开，要一直保持这个链接, 长连接
    //  后面有其他方案！
    char buffer[1024];
    while (true)
    {
        // read && write 可以直接被使用！
        ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = 0; // 将发过来的数据当做字符串
            std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
        }
        else if (s == 0) // 对端关闭连接
        {
            logMessage(NORMAL, "%s:%d shutdown, me too!", clientip.c_str(), clientport);
            break;
        }
        else
        { //
            logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
            break;
        }

        write(sock, buffer, strlen(buffer));
    }
    close(sock);
}

static void change(int sock, const std::string &clientip,
                   const uint16_t &clientport, const std::string &thread_name)
{
    // echo server
    //  同时在线10人
    //  所以，我们一般服务器进程业务处理，如果是从连上，到断开，要一直保持这个链接, 长连接
    //  后面有其他方案！
    char buffer[1024];
    // read && write 可以直接被使用！
    ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
    if (s > 0)
    {
        buffer[s] = 0; // 将发过来的数据当做字符串
        std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
        std::string message;
        char *start = buffer;
        while (*start)
        {
            char c;
            if (islower(*start))
                c = toupper(*start);
            else
                c = *start;
            message.push_back(c);
            start++;
        }

        write(sock, message.c_str(), message.size());
    }
    else if (s == 0) // 对端关闭连接
    {
        logMessage(NORMAL, "%s:%d shutdown, me too!", clientip.c_str(), clientport);
    }
    else
    { //
        logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
    }

    close(sock);
}
static void dictOnline(int sock, const std::string &clientip,
                       const uint16_t &clientport, const std::string &thread_name)
{
    // echo server
    //  同时在线10人
    //  所以，我们一般服务器进程业务处理，如果是从连上，到断开，要一直保持这个链接, 长连接
    //  后面有其他方案！
    char buffer[1024];
    static std::unordered_map<std::string, std::string> dict = {
        {"apple", "苹果"},
        {"bite", "比特"},
        {"banana", "香蕉"},
        {"hard", "好难啊"}};
    while (true)
    {
        // read && write 可以直接被使用！
        ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = 0; // 将发过来的数据当做字符串
            std::cout << thread_name << "|" << clientip << ":" << clientport << "# " << buffer << std::endl;
            std::string message;
            auto iter = dict.find(buffer);
            if (iter == dict.end())
                message = "我不知道...";
            else
                message = iter->second;
            write(sock, message.c_str(), message.size());
        }
        else if (s == 0) // 对端关闭连接
        {
            logMessage(NORMAL, "%s:%d shutdown, me too!", clientip.c_str(), clientport);
            break;
        }
        else
        { //
            logMessage(ERROR, "read socket error, %d:%s", errno, strerror(errno));
            break;
        }
    }

    close(sock);
}

// void service(int serverSock, const std::string client_ip, uint16_t client_port)
// {
//     char buffer[1024];
//     while (true)
//     {
//         ssize_t s = read(serverSock, buffer, sizeof(buffer) - 1);

//         if (s > 0)
//         {
//             buffer[s] = '\0';
//             std::cout << "|--" << client_ip << ":" << client_port << "# " << buffer << std::endl;
//         }
//         else if (s == 0)
//         {
//             break;
//         }
//         else
//         {
//             break;
//         }
//         write(serverSock, buffer, sizeof buffer);
//     }
//     close(serverSock);
// }

// static void* threadRoutine(void* args)
// {
//     ThreadData* td = (ThreadData*)args;

//     service(td->_sock, td->_ip, td->_port);
// }

class TcpServer
{
private:
public:
    TcpServer(uint16_t port, std::string ip = "")
        : _ip(ip), _port(port), _threadpool_ptr(ThreadPool<Task>::GetThreadPool())
    {
    }

    void InitServer()
    {
        // 创建监听套接字
        _listensock = socket(AF_INET, SOCK_STREAM, 0);
        if (_listensock < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(2);
        }

        struct sockaddr_in server;
        bzero(&server, sizeof server);

        server.sin_family = AF_INET;
        server.sin_port = htons(_port);
        server.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());

        if (bind(_listensock, (struct sockaddr *)&server, sizeof server) < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(2);
        }

        if (listen(_listensock, 20) < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(2);
        }

        logMessage(NORMAL, "tcp_server is init ! !");
    }

    void Start()
    {
        // signal(SIGCHLD, SIG_IGN);
        _threadpool_ptr->run();

        while (true)
        {
            struct sockaddr_in src;
            bzero(&src, sizeof src);
            socklen_t len = sizeof src;

            int serverSockt = 0;
            if ((serverSockt = accept(_listensock, (struct sockaddr *)&src, &len)) < 0)
            {
                logMessage(ERROR, "%d:%s", errno, strerror(errno));
                continue;
            }
            std::cout << _listensock << std::endl;

            uint16_t client_port = ntohs(src.sin_port);
            std::string client_ip = inet_ntoa(src.sin_addr);

            logMessage(NORMAL, "link success, servicesock: %d | %s : %d |\n",
                       serverSockt, client_ip.c_str(), client_port);

            // version 1.0
            // service(serverSockt, client_ip, client_port);

            // version 2.0 -- 多进程版 --- 创建子进程
            // 让子进程给新的连接提供服务，子进程能不能打开父进程曾经打开的文件fd呢？1 0
            // pid_t id = fork();
            // assert(id != -1);
            // if(id == 0)
            // {
            //     // 子进程， 子进程会不会继承父进程打开的文件与文件fd呢？1, 0
            //     // 子进程是来进行提供服务的，需不需要知道监听socket呢？
            //     close(_listensock);
            //     service(serverSockt, client_ip, client_port);
            //     exit(0); // 僵尸状态 忽略子进程发送的信号会自动释放资源
            // }

            // version2.1 -- 多进程版
            // pid_t id = fork();
            // if(id == 0)
            // {
            //     // 子进程
            //     close(_listensock);
            //     if(fork() > 0/*子进程本身*/) exit(0); //子进程本身立即退出
            //     // 孙子进程， 孤儿进程，OS领养， OS在孤儿进程退出的时候，由OS自动回收孤儿进程！
            //     service(serverSockt, client_ip, client_port);
            //     exit(0);
            // }
            // // 父进程
            // waitpid(id, nullptr, 0); //不会阻塞！
            // close(serverSockt);

            // version 3 --- 多线程版本
            // ThreadData *td = new ThreadData();
            // td->_sock = serverSockt;
            // td->_ip = client_ip;
            // td->_port = client_port;
            // pthread_t tid;
            // // 在多线程这里用不用进程关闭特定的文件描述符呢？？1 0 不用
            // pthread_create(&tid, nullptr, threadRoutine, td);

            // verison4 --- 线程池版本
            // Task t(serverSockt, client_ip, client_port, service);
            // Task t(serverSockt, client_ip, client_port, change);
            Task t(serverSockt, client_ip, client_port, dictOnline);
            _threadpool_ptr->pushTask(t);
        }
    }

    ~TcpServer()
    {
        close(_listensock);
    }

private:
    int _listensock;
    std::string _ip;
    uint16_t _port;
    std::unique_ptr<ThreadPool<Task>> _threadpool_ptr;
};