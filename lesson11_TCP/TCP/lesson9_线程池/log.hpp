#pragma once

#include <iostream>
#include <string>
#include <ctime>
#include <cstdarg>
#include <cstdio>

// 日志等级
#define DEBUG    0
#define NORMAL   1
#define WARNING  2
#define ERROR    3
#define FATAL    4
// 日志等级字符映射表
const char* gLevel[] = {
    "DEBUG",
    "NORMAL",
    "WARNING",
    "ERROR",
    "FATAL"
};

#define LOGFILE "./threadpool.log"
// 日志基本要素：日志等级 日志时间 日志内容
void logMessage(int level, const char* format, ... )
{
#ifndef DEBUG_SHOW
    if (level == DEBUG) return;
#endif
    char stdBuffer[1024];// 标准
    time_t timestamp = time(nullptr);
    struct tm* curtm = gmtime(&timestamp);

    snprintf(stdBuffer, sizeof stdBuffer, "[%s] [%d年%d月%d日%d时%d分%d秒] ", gLevel[level], 
    curtm->tm_year + 1900, curtm->tm_mon + 1, curtm->tm_mday, curtm->tm_hour + 1 + 7 % 24, curtm->tm_min, curtm->tm_sec);

    char logBuffer[1024];// 自定义
    va_list args;
    va_start(args, format);
    vsnprintf(logBuffer, sizeof logBuffer, format, args);
    va_end(args);

    printf("%s--%s\n", stdBuffer, logBuffer);

    // FILE* fp = fopen(LOGFILE, "a");

    // fprintf(fp, "%s--%s\n", stdBuffer, logBuffer);

    // fclose(fp);
}