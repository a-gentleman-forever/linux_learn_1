#pragma once

#include <pthread.h>

class Mutex
{
public:
    Mutex(pthread_mutex_t *mtx)
        : _mtx(mtx)
    {
    }

    void lock()
    {
        pthread_mutex_lock(_mtx);
    }

    void unlock()
    {
        pthread_mutex_unlock(_mtx);
    }

    ~Mutex()
    {
    }

private:
    pthread_mutex_t *_mtx;
};

class lockGuard
{
public:
    lockGuard(pthread_mutex_t *mtx)
        : _mtx(mtx)
    {
        _mtx.lock();
    }

    ~lockGuard()
    {
        _mtx.unlock();
    }

private:
    Mutex _mtx;
};