#pragma once 

#include <iostream>
#include <string>
#include <pthread.h>

typedef void*(*fun_t)(void*);

class ThreadData
{
public:
public:
    std::string _name;
    void* _args;
};

class Thread
{
public:
    Thread(int num, fun_t callback, void* args)
    :_func(callback)
    {
        char nameBuffer[64];
        snprintf(nameBuffer, sizeof nameBuffer, "Thread %d", num + 1);
        _name = nameBuffer;

        _tData._args = args;
        _tData._name = _name;
    }

    void start()
    {
        pthread_create(&tid, nullptr, _func, (void*)&_tData);
    }

    void join()
    {
        pthread_join(tid, nullptr);
    }

    ~Thread()
    {}
private:
    std::string _name;
    pthread_t tid;
    fun_t _func;
    ThreadData _tData;
};
