#include "Task.hpp"
#include "thread.hpp"
#include "threadPool.hpp"
#include "log.hpp"
#include <ctime>
#include <unistd.h>
#include <iostream>

int main()
{
    srand((unsigned int)time(nullptr));
    ThreadPool<Task>::getThreadPool()->run();

    while (true)
    {
        int x = rand() % 100;
        int y = rand() % 100;
        Task t(x, y, [](int x, int y)->int {
            return  x + y;
        });

        ThreadPool<Task>::getThreadPool()->pushTask(t);

        sleep(1);
    }

    ThreadPool<Task>::getThreadPool()->joins();

    return 0;
}