#include <iostream>
#include <memory>
#include "tcp_server.hpp"

void Usage(std::string filename)
{
    std::cout << "./Usage " << filename << " port" << std::endl;
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }

    uint16_t port = atoi(argv[1]);

    std::unique_ptr<TcpServer> server(new TcpServer(port));

    server->InitServer();
    server->Start();


    return 0;
}
