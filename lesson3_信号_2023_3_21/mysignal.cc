#include <iostream>
#include <unistd.h>
#include <signal.h>

void CatchSig(int signum)
{
    std::cout << "获得了一个信号：" << signum << std::endl;
}
static void showpending(sigset_t& pending)
{
    for (int i = 1; i <= 31; ++i)
    {
        if (sigismember(&pending, i))
        {
            std::cout << "1";
        }  
        else
        {
            std::cout << "0";
        }
    }
        std::cout << " pid:" << getpid() << std::endl;
}

static void blockSig(int sig)
{
    sigset_t bset, obset;
    sigemptyset(&bset);
    sigemptyset(&obset);
    //添加阻塞信号
    sigaddset(&bset, sig);
    //写入进程的block中
    sigprocmask(SIG_BLOCK, &bset, &obset);
}

int main()
{
    for (int i = 0; i <= 31; ++i)
    {
        blockSig(i);
    }

    sigset_t pending;
    while (true)
    {
        sigpending(&pending);
        showpending(pending);
        sleep(1);
    }

    // signal(2, CatchSig);

    // sigset_t bset, obset;
    // sigemptyset(&bset);
    // sigemptyset(&obset);
    // //添加阻塞信号
    // sigaddset(&bset, 2);
    // //写入进程的block中
    // sigprocmask(SIG_BLOCK, &bset, &obset);

    // sigset_t pending;
    // int count = 0;
    // while (true)
    // {
    //     sigpending(&pending);
    //     showpending(pending);
    //     sleep(1);

    //     count++;
    //     if (count == 15)
    //     {
    //         sigprocmask(SIG_SETMASK, &obset, nullptr);

    //         std::cout << "解除2号信号限制" << std::endl;
    //     }
    // }

    // for (int i = 1; i <= 31; ++i)
    // {
    //     signal(i, CatchSig);
    // }

    // while (true) sleep(1);

    return 0;
}