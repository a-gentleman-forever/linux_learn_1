#include "game.h"

void menu()
{
  printf("*******************************\n");
  printf("**** 1.play   0.end ***********\n");
  printf("*******************************\n");
  
  printf("Please Select:");
}

int x,y;

void BordMove(int bord[][COL], int cow, int col, int who)
{
  printf("PLAYER[%d] is play:", who);
  while (1)
  {
  scanf("%d %d", &x, &y);

  // 检测合理
  if (x > cow  || x < 1 || y < 1 || y > col || bord[x - 1][y - 1] != 0)
  {
    printf("Please Input:");
  }
  else 
  {
    bord[x - 1][y - 1] = who;
    break;
  }
  }
}

int chesscount(int bord[][COL], int cow, int col, enum Dir d)
{
  int _x = x - 1;
  int _y = y - 1;
  int count = 0;

  while (1)
  {
  switch (d)
  {
    case LEFT:
      _x++;
      break;
    case RIGHT:
      _x--;
      break;
    case UP:
      _y++;
      break;
    case DOWN:
      _y--;
      break;
    case LEFT_UP:
      _x++;
      _y++;
      break;
    case LEFT_DOWN:
      _x++;
      _y--;
      break;
    case RIGHT_UP:
      _x--;
      _y++;
      break;
    case RIGHT_DOWN:
      _x--;
      _y--;
      break;
    default:
      break;
  }
  
  if (_x < 0 || _x >= cow || _y < 0 || _y >= col || bord[_x][_y] != bord[x - 1][y - 1])
  {
    break;
  }
  else 
  {
    count++;
  }

  }

  return count;
}

int isOver(int bord[][COL], int cow, int col)
{
  int count1 = chesscount(bord, cow, col, LEFT) + chesscount(bord, cow, col, RIGHT) + 1;
  int count2 = chesscount(bord, cow, col, UP) + chesscount(bord, cow, col, DOWN) + 1;
  int count3 = chesscount(bord, cow, col, LEFT_UP) + chesscount(bord, cow, col, RIGHT_DOWN) + 1;
  int count4 = chesscount(bord, cow, col, RIGHT_UP) + chesscount(bord, cow, col, LEFT_DOWN) + 1;

  if (count1 >= 5 || count2 >= 5 || count3 >= 5 || count4 >= 5)
  {
    return bord[x - 1][y - 1];
  }

  int i = 0;
  for (i = 0; i < cow; i++)
  {
    int j = 0;
    for (j = 0; j < col; j++)
    {
      if (bord[i][j] == 0)
      {
          return NEXT;
      }
    }
  }

  return DROW;
}

void ShowBord(int bord[][COL], int cow, int col)
{
  printf("\x1b[H\x1b[2J");

  int i = 0;
  printf("   ");
  for (i = 1; i <= col; i++)
  {
    printf("%3d", i);
  }
  printf("\n");

  for (i = 0; i < cow; i++)
  {
    printf("%3d ", i + 1);
    int j = 0;
    for (;j < col; j++)
    {
      if (bord[i][j] == PLAYER1)
        printf(" ● ");
      else if (bord[i][j] == PLAYER2)
        printf(" ○ ");
      else 
        printf(" . ");
    }
    printf("\n");
  }
}

void game()
{
  int result = 0;
  // 初始化棋盘
  int playbord[COW][COL];
  memset(playbord, 0, sizeof(playbord));
  
  while (1)
  {
  // 玩家1下棋
  ShowBord(playbord, COW, COL);
  BordMove(playbord, COW, COL, PLAYER1);
  result = isOver(playbord, COW, COL);
  if (result !=  NEXT)
  {
    break;
  }
  // 玩家2下棋
  ShowBord(playbord, COW, COL);
  BordMove(playbord, COW, COL, PLAYER2);
  result = isOver(playbord, COW, COL);
  if (result != NEXT)
  {
    break;
  }
  }

  ShowBord(playbord, COW, COL);
  if (result == PLAYER1_WIN)
    printf("PLAYER1 is WIN!!!\n");
  else 
    printf("PLAYER2 is WIN!!!\n");
}

