#include  <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COW 20
#define COL 20

#define NEXT 0

#define PLAYER1 1
#define PLAYER2 2

#define PLAYER1_WIN 1
#define PLAYER2_WIN 2
#define DROW 3

enum Dir
{
  LEFT,
  RIGHT,
  UP,
  DOWN,
  LEFT_UP,
  LEFT_DOWN,
  RIGHT_UP,
  RIGHT_DOWN
};

void menu();
void game();

