#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int g_unval;
int g_val = 100;

int main(int argc, char* argv[], char* env[])
{   
    int i = 0;

    printf("code addr: %p\n", main);   //代码段
    printf("init global addr: %p\n", &g_val); //初始化全局区

    printf("uninit global addr: %p\n", &g_unval); //未初始化全局区

    char* heap_mem = (char*)malloc(10);
    printf("heap addr: %p", heap_mem); //堆区
    printf("stack addr: %p\n", &heap_mem);

    for (i = 0; i <argc; ++i)
    {
        printf("argc[%d]: %p\n", i, argv[i]);
    }
    
    for (i = 0; env[i]; ++i)
    {
        printf("env[%d]: %p\n", i, env[i]);
    }
    

    return 0;
}
